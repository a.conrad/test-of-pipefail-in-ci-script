#!/bin/bash

# `cat foobar` will fail, as the file doesn't exist

function func_with_pipe() {
	cat foobar | wc -l
}

which bash
bash --version

echo "test w/o pipefail outside of func"
cat foobar | wc -l

echo "test w/o pipefail inside of func"
func_with_pipe

echo "setting pipefail"
set -eo pipefail

echo "test pipefail inside of func"
func_with_pipe

echo "the above should fail, so we shouldn't get this far"

echo "test pipefail outside of func"
cat foobar | wc -l
